FROM python:3.8-buster

# File Author / Maintainer
MAINTAINER Mathieu Courcelles

RUN mkdir -p /usr/app
WORKDIR /usr/app

# Prepare Python environment
RUN pip install -v --no-cache-dir pipenv
COPY Pipfile* /usr/app/
RUN /usr/local/bin/pipenv install --system --deploy

# Prepare models directory for www-data user
RUN mkdir -p /var/www
RUN chown www-data /var/www

USER www-data

# Download our datasets and trained models
RUN mhcflurry-downloads fetch models_class1_presentation

CMD ["mhcflurry-predict"]